﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Advanced
{
    public interface IUnitOfWork
    {
        public IRepository<Order> Orders { get;}
        public IRepository<User> Users { get;}
        public IRepository<Product> Products { get;}
    }
}
