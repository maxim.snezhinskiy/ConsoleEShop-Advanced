﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Advanced
{
    public interface IMenu
    {
        public EventHandler LoginNotify { get; set; }
        public EventHandler LogOutNotify { get; set; }
        public void Execute();
    }
}
