﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace ConsoleEShop_Advanced
{
    public class OrderRepository : IEnumerable<Order>, IRepository<Order>
    {
        public DbContext db;

        public int Count => db.Orders.Count;

        public Order this[int index]
        {
            get
            {
                if (index < 0 || index > db.Orders.Count)
                {
                    throw new ArgumentException($"index is > {db.Orders.Count}");
                }
                return db.Orders[index];
            }
            set
            {
                if (index < 0 || index > db.Orders.Count)
                {
                    throw new ArgumentException($"index is > {db.Orders.Count}");
                }
                if (value == null)
                {
                    throw new ArgumentNullException("", new Exception());
                }
                db.Orders[index] = value;
            }
        }
    
        public OrderRepository(DbContext context)
        {
            db = context;
        }
  


        public Order Find(Predicate<Order> p) 
        {
            return db.Orders.Find(p);
        }



        public IEnumerator<Order> GetEnumerator()
        {
            return ((IEnumerable<Order>)this).GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return db.Orders.GetEnumerator();
        }

        public bool Create(Order item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("", new Exception());
            }
            if (db.Orders.Contains(item))
                return false;

            db.Orders.Add(item);
            return true;
        }

        public Order Get(int id)
        {
            return db.Orders.FirstOrDefault(i=> i.Id == id);
        }

        public List<Order> Get()
        {
            return db.Orders;
        }

        public void Update(Order item)
        {
            int indexToUpdate = db.Orders.Select(x => x.Id).FirstOrDefault(x => x == item.Id) - 1;

            if (indexToUpdate != -1)
            {
                db.Orders[indexToUpdate] = item;
            }

        }

        public void Delete(int id)
        {
            if (!(db.Orders.Exists(i => i.Id == id)))
            {
                throw new ArgumentException(nameof(this.Delete), new Exception("User with such ID is not exist"));
            }
            db.Orders.Remove(db.Orders.Find(i => i.Id == id));
        }
    }
}
