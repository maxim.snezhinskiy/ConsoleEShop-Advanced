﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace ConsoleEShop_Advanced
{
    public class ProductRepository : IEnumerable<Product>, IRepository<Product>
    {

        public DbContext db;

        public int Count { get => db.Products.Count; }

        public Product this[int index]
        {
            get
            {
                if (index < 0 || index > Count)
                {
                    throw new ArgumentException($"index is > {Count}");
                }
                return db.Products[index];
            }
            set
            {
                if (index < 0 || index > Count)
                {
                    throw new ArgumentException($"index is > {Count}");
                }
                if (value == null)
                {
                    throw new ArgumentNullException("", new Exception());
                }
                db.Products[index] = value;
            }
        }

        public ProductRepository(DbContext context)
        {
            db = context;
        }


        public Product Find(Predicate<Product> p)
        {
            return db.Products.Find(p);
        }
   

        public override string ToString()
        {
            string res = "";
            foreach (var i in db.Products)
            {
                res += i.ToString();
            }
            return res;
        }


        public IEnumerator GetEnumerator()
        {
            return db.Products.GetEnumerator();
        }
        IEnumerator<Product> IEnumerable<Product>.GetEnumerator()
        {
            return ((IEnumerable<Product>)db.Products).GetEnumerator();
        }

        public bool Create(Product item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(this.Create), new Exception());

            if (db.Products.Contains(item))
                return false;

            db.Products.Add(item);
            return true;
        }

        public Product Get(int id)
        {
            return db.Products.FirstOrDefault(i=> i.Id == id);
        }

        public List<Product> Get()
        {
            return db.Products;
         
        }

        public void Update(Product item)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            if(!(db.Products.Exists(i=> i.Id == id))) 
            {
                throw new ArgumentException(nameof(this.Delete), new Exception("Product with such ID is not exist"));
            }
            db.Products.Remove(db.Products.Find(i=>i.Id==id));
        }
    }
}

