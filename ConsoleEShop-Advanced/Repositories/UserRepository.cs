﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace ConsoleEShop_Advanced
{
    public class UserRepository : IEnumerable<User>, IRepository<User>
    {

        public DbContext db;
        public int Count { get => db.Users.Count; }

        public UserRepository(DbContext context) 
        {
            db = context;
        }
        public User this[int index]
        {
            get
            {
                if (index < 0 || index > Count)
                {
                    throw new ArgumentException($"index is > {Count}");
                }
                return db.Users[index];
            }
            set
            {
                if (index < 0 || index > Count)
                {
                    throw new ArgumentException($"index is > {Count}");
                }
                if (value == null)
                {
                    throw new ArgumentNullException("", new Exception());
                }
                db.Users[index] = value;
            }
        }


        public User Find(Predicate<User> p) 
        {
            return db.Users.Find(p);
        }

        public bool UserExist(string login) 
        {
            return db.Users.Exists(i=> i.Login == login);
        }

        public bool UserExist(string login, string password)
        {
            return db.Users.Exists(i => i.Login == login && i.Password == password);
        }
        public IEnumerator<User> GetEnumerator()
        {
            return db.Users.GetEnumerator() as IEnumerator<User>;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return db.Users.GetEnumerator();
        }

        public bool Create(User item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("", new Exception());
            }
            if (UserExist(item.Login, item.Password))
                return false;
            db.Users.Add(item);
            return true;
        }

        public User Get(int id)
        {
            return db.Users.FirstOrDefault(i => i.Id == id);
        }

        public List<User> Get()
        {
            return db.Users;
        }

        public void Update(User item)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            if (!(db.Users.Exists(i => i.Id == id)))
            {
                throw new ArgumentException(nameof(this.Delete), new Exception("User with such ID is not exist"));
            }
            db.Users.Remove(db.Users.Find(i => i.Id == id));
        }
    }
}
