﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace ConsoleEShop_Advanced
{
    public class Order :IEntity
    {
        private static int ID;
        public Address OrderAddress { get; set; }
        public List<OrderItem> Items { get; set; }
        public OrderStatus OrderStatus { get;  set; }
        public bool isConfirmed { get; set; }

        public int UserID { get; private set; }

        public int Id { get; }

        public Order()
        {
            ID++;
            Id = ID;
            Items = new List<OrderItem>();
            OrderStatus = OrderStatus.New;
            OrderAddress = new Address();
        }

        public Order(OrderItem item, Address address, int userId) : this()
        {

            Items.Add(item);
            OrderAddress = address;
            UserID = userId;
        }

        public Order(Address address, int userId, params OrderItem[] items) : this()
        {
            Items.AddRange(items);
            OrderAddress = address;
            UserID = userId;
        }

        public Order(Address address, int userId, IEnumerable<OrderItem> items) : this()
        {
            Items = new List<OrderItem>(items);
            OrderAddress = address;
            UserID = userId;
        }

        public override bool Equals(object obj)
        {
            if (obj != null && obj is Order o)
            {
                return this.Id != o.Id;
            }
            return false;
        }

        public void AddOrderItem(OrderItem item)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(this.AddOrderItem), new Exception());
            }
            Items?.Add(item);
        }
        public List<OrderItem> GetOrderItems() => Items;

        public override string ToString()
        {
            string items = "";
            foreach (var i in Items)
            {
                items +=i+"\n";
            }
            return $"-- Order №{this.Id}--\n" +
                   $"{items}\n" +
                   $"Order status: {OrderStatus.ToString()}\n"+
                   $"{OrderAddress}" +
                   $"\n-- Total price: {Items.Sum(i => i.Product.Price * i.Quantity)}\n";
        }
    }
}
