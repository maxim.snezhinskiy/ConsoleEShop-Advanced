﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Advanced
{
    public class DataSource
    {
        public static List<Product> Products { get; set; } = new List<Product>()
        {
             new Product("Iphone 12", 1199, "Apple Iphone",  new Category("Phones")),
             new Product("Samsung Galaxy s41", 999, "Samsung corporation",  new Category("Phones")),
             new Product("Mercedes benz E-200", 68999, "The best car ever",  new Category("Cars")),
             new Product("Apple airPods", 199, "Apple Inc", new Category("HeadPhones"))
        };

        public static List<User> Users { get; set; } = new List<User>()
        {
                new RegisteredUser("simpleUSer", "password"),
                new RegisteredUser("user1", "user1"),
                new Admin("admin", "admin")
        };

        public static List<Order> Orders { get; set; } = new List<Order>();
    }
}
