﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Advanced
{
    public class OrderItem
    {
        public int Quantity { get; set; }
        public Product Product { get; set; }
        public OrderItem()
        {

        }
        public OrderItem(Product product, int quantity)
        {
            Product = product;
            Quantity = quantity;
        }

        public override string ToString()
        {
            return $"\n-{Product}\nQuantity: {Quantity}";
        }


    }
}
